package dev.polarian.cbnotifier;

public enum EventType {
    DEATHMATCH,
    FREE_SIEGE_BATTLES,
    FREE_FIELD_BATTLES,
    TERRITORY_WAR,
     RANKED_SIEGE
}
