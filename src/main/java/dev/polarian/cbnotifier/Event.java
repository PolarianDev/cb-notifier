package dev.polarian.cbnotifier;

public class Event {
    private EventType event;
    private int startMins;
    private int endMins;
    private boolean hasStarted;
    private boolean fiveMinWarn;
    private boolean fithteenMinWarn;

    Event(EventType event, String start, String end) {
        this.event = event;
        this.startMins = calculateMins(start);
        this.endMins = calculateMins(end);
        this.hasStarted = false;
        this.fiveMinWarn = false;
        this.fithteenMinWarn = false;
    }

    private int calculateMins(String time) {
        return (Integer.parseInt(time.substring(0, 2)) * 60) + Integer.parseInt(time.substring(3, 5));
    }

    public EventType getEvent() {
        return event;
    }

    public int getStartMins() {
        return startMins;
    }

    public int getEndMins() {
        return endMins;
    }

    public boolean hasStarted() {
        return hasStarted;
    }

    public boolean hasFiveMinWarned() {
        return fiveMinWarn;
    }

    public boolean hasFithteenMinWarned() {
        return fithteenMinWarn;
    }

    public void setFithteenMinWarned(boolean bool) {
        this.fithteenMinWarn = bool;
    }

    public void setFiveMinWarned(boolean bool) {
        this.fiveMinWarn = bool;
    }

    public void setHasStarted(boolean bool) {
        this.hasStarted = bool;
    }
}
