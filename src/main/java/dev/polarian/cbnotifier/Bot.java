package dev.polarian.cbnotifier;

import club.minnced.discord.webhook.WebhookClient;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class Bot {
    public static final Logger logger = LoggerFactory.getLogger(Bot.class);

    private static HashMap<String, Event[]> loadEvents() {
        HashMap<String, Event[]> events = new HashMap<>();
        String[] days = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        Path path = Path.of("events.json");
        if (!Files.exists(path)) {
            logger.error("Could not find event json, exiting...");
            System.exit(1);
        }

        String data = null;
        try {
            data = Files.readString(path);
        } catch (IOException error) {
           error.printStackTrace();
           System.exit(1);
        }
        JsonObject parser = JsonParser.parseString(data).getAsJsonObject();
        for (String day : days) {
           JsonArray dayArray = parser.getAsJsonArray(day);
           Event[] dayEvents = new Event[dayArray.size()];
           for (int i = 0; i < dayArray.size(); i++) {
               JsonObject event = dayArray.get(i).getAsJsonObject();
               EventType type;
               switch (event.get("name").getAsString()) {
                   case "Deathmatch":
                       type = EventType.DEATHMATCH;
                       break;
                   case "Free Siege Battles":
                       type = EventType.FREE_SIEGE_BATTLES;
                       break;
                   case "Free Field Battles":
                       type = EventType.FREE_FIELD_BATTLES;
                       break;
                   case "Territory War":
                       type = EventType.TERRITORY_WAR;
                       break;
                   case "Ranked Siege":
                       type = EventType.RANKED_SIEGE;
                       break;
                   default:
                       throw new IllegalStateException("Unexpected value: " + event.get("name").toString());
               }
               Event eventObject = new Event(type, event.get("start").getAsString(), event.get("end").getAsString());
               dayEvents[i] = eventObject;
           }
           events.put(day, dayEvents);
        }
        return events;
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            logger.error("Invalid number of parameters passed...");
            logger.info("Correct usage: ReminderBot <webhook> <event check in seconds>");
            System.exit(1);
        }
        String webhook = args[0];
        WebhookClient client = WebhookClient.withUrl(webhook);
        long eventCheck = Long.parseLong(args[1]);
        HashMap<String, Event[]> events = loadEvents();

        Timer timer = new Timer("Event-Checker");
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                logger.info("Beginning event check");
                String time = new SimpleDateFormat("EE hh mm").format(new Date(System.currentTimeMillis()));
                Event[] daysEvents = events.get(time.substring(0, 3));
                for (Event event : daysEvents) {
                    int currentMins = (Integer.parseInt(time.substring(4, 6)) * 60) + Integer.parseInt(time.substring(7, 9));
                    int eventMins = event.getStartMins();
                    int remaining = eventMins - currentMins;

                    if (remaining <= 0 && !event.hasStarted()) {
                        // event starting alert

                        // set started warning
                        event.setHasStarted(true);
                        break;
                    } else if (remaining <= 15 && !event.hasFithteenMinWarned()) {
                        // do 15 min warning

                        // set warning to true
                        event.setFithteenMinWarned(true);
                        break;
                    } else if (remaining <= 5 && !event.hasFiveMinWarned()) {
                        // do 5 min warning

                        // set warning to true
                        event.setFiveMinWarned(true);
                        break;
                    } else if (event.getEndMins() - event.getStartMins() <= 0) {
                        // event end message

                        // reset variables
                        event.setHasStarted(false);
                        event.setFithteenMinWarned(false);
                        event.setFiveMinWarned(false);
                    }
                }
                logger.info("Finished event check");
            }
        };
        timer.scheduleAtFixedRate(task, 0, eventCheck * 1000L);

    }
}
